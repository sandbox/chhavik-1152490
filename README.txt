/* $Id: README.txt, v 1.0 2011/05/11 cck combo $ */

Description
-----------
This is a very efficent module that allows to add a 'combo' field to your content types.
It provides two widgets on choosing the 'CCK Combo' field:-
1) 'Node Reference + Textfield'
2) 'User Reference + Textfield'

Installation
------------
 * Copy CCK Combo module to your module directory and then enable on the admin
   modules page.
 * Add a 'CCK Combo' field to your content type.
 * Choose from the available widgets and make the settings.
 * Now, the field is visible on all the nodes of the specified content type.

Author
------
Chhavi Kaushik
chhavi@srijan.in
